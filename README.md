Docs: https://nullfati.gitlab.io/task-manager-stm32duino/

Latests firmware:
- [Black Pill V2 (STM32 F411CE)](https://gitlab.com/Nullfati/task-manager-stm32duino/-/jobs/artifacts/master/download?job=build_blackpill_f411ce)
- [Lolin32](https://gitlab.com/Nullfati/task-manager-stm32duino/-/jobs/artifacts/master/download?job=build_lolin32)

Deprecated:

Before build run this command in the root directory of the project to install all needed dependencies:
```
pio lib install lib/*/
```
