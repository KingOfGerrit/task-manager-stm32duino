#pragma once

#include "Bluetooth.h"

#include <project_config.h>

namespace bluetooth
{

class BluetoothGlobal final: public Bluetooth
{
private:
    explicit BluetoothGlobal(std::unique_ptr<bluetooth_impl::BluetoothImpl> impl)
        : Bluetooth(std::move(impl))
    {
    }

public:
    BluetoothGlobal(BluetoothGlobal &other) = delete;
    void operator=(const BluetoothGlobal &) = delete;

    static BluetoothGlobal &getInstance()
    {
        static BluetoothGlobal instance(
            bluetooth_impl::createDefault());
        return instance;
    }

    static BluetoothResult readDataDefault()
    {
        return getInstance().readData(project_cfg::bluetooth_global_size_of_json);
    }
};

}
