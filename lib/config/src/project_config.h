#pragma once

namespace project_cfg
{

extern const int build_version;

extern const int bluetooth_global_tx;
extern const int bluetooth_global_rx;
extern const int bluetooth_global_serial_speed;
extern const int bluetooth_global_size_of_json;

extern const int task_manager_task_count;

extern const char *rtc_module;

}
