#pragma once

#include <cstdint>

#include <Task.hpp>

struct SetValueInEepromInput : public TaskInput
{
	int address;
	uint8_t value;

	bool fillData(ArduinoJson::JsonObjectConst json) override;
};

class SetValueInEeprom : public Task
{
public:
	SetValueInEeprom(CountOfExecute countOfExecute, TaskInput *taskInput);

	void run() override;
};
