.. Task manager STM32Duino documentation master file, created by
   sphinx-quickstart on Tue Jun  6 22:58:14 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Task manager STM32Duino's documentation!
===================================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   general
   libs_docs_link

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
