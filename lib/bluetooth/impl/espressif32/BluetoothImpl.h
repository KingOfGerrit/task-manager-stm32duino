#pragma once

#include "BluetoothTypes.h"

#include <memory>

#include <ArduinoJson.hpp>
#include <BluetoothSerial.h>

namespace bluetooth_impl
{

class BluetoothImpl
{
public:
    BluetoothImpl();

    bluetooth::BluetoothResult readData(size_t sizeOfJson);
    void writeData(const String &data);
    void writeData(ArduinoJson::JsonObjectConst jsonObject);
    void writeData(const ArduinoJson::JsonDocument &jsonDocument);
};

inline std::unique_ptr<BluetoothImpl> createDefault()
{
    return std::make_unique<BluetoothImpl>();
}

}
