#pragma once

#include "BluetoothTypes.h"

#include <memory>

#include <ArduinoJson.hpp>

#include <project_config.h>

namespace bluetooth_impl
{

class BluetoothImpl
{
public:
    BluetoothImpl(int rx, int tx, int serialSpeed);

    bluetooth::BluetoothResult readData(size_t sizeOfJson);
    void writeData(const String &data);
    void writeData(ArduinoJson::JsonObjectConst jsonObject);
    void writeData(const ArduinoJson::JsonDocument &jsonDocument);

private:
    HardwareSerial m_serial;
};

inline std::unique_ptr<BluetoothImpl> createDefault()
{
    return std::make_unique<BluetoothImpl>(
        project_cfg::bluetooth_global_rx,
        project_cfg::bluetooth_global_tx,
        project_cfg::bluetooth_global_serial_speed
    );
}

}
