#pragma once

#include <Arduino.h>

#include <ArduinoJson.hpp>

namespace bluetooth
{

enum class BluetoothError
{
    Unknown,
    NoError,
    NoDataAvailable,
    DeserializationError,
};

struct BluetoothResult
{
    explicit BluetoothResult(int sizeOfStoredJson)
        : sizeOfJson(sizeOfStoredJson), data(sizeOfJson)
    {}

    size_t sizeOfJson;
    ArduinoJson::DynamicJsonDocument data;

    BluetoothError error = BluetoothError::Unknown;
    ArduinoJson::DeserializationError deserializationError = ArduinoJson::DeserializationError::Ok;
};

}
