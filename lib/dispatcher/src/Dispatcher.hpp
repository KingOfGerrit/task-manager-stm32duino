#pragma once

#include <functional>
#include <list>

template <typename... Args>
class Dispatcher
{
public:
	using CallBackType = std::function<void(Args...)>;

	class CallBackId
	{
	public:
		CallBackId() : valid(false) {}
	private:
		friend class Dispatcher<Args...>;
		CallBackId(typename std::list<CallBackType>::iterator i)
			: iter(i), valid(true)
		{}

		typename std::list<CallBackType>::iterator iter;
		bool valid;
	};

	// register to be notified
	CallBackId addCallBack(CallBackType cb)
	{
		if (cb)
		{
			cbs.push_back(cb);
			return CallBackId(--cbs.end());
		}
		return CallBackId();
	}

	// unregister to be notified
	void delCallBack(CallBackId &id)
	{
		if (id.valid)
		{
			cbs.erase(id.iter);
		}
	}

	void broadcast(Args... args)
	{
		for (auto &cb : cbs)
		{
			cb(args...);
		}
	}

private:
	std::list<CallBackType> cbs;
};
