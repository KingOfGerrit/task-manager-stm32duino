import os
import CppHeaderParser
import json
import subprocess

Import("env")


def find_library(path: str, name: str) -> str:
    search_path = os.path.realpath(path)
    file_type = "json"
    search_str = name

    for root, dirs, files in os.walk(search_path):
        for file in files:
            if file.endswith("." + file_type):
                print('Find library: ' + os.path.join(root, file))
                with open(os.path.join(root, file)) as f:
                    contents = f.read()
                    if search_str in contents:
                        return root

    return ""


def header_to_json(header: str, save_dir: str, file_name: str) -> None:
    try:
        cppHeader = CppHeaderParser.CppHeader(header)
    except CppHeaderParser.CppParseError as e:
        raise e

    data_set = {}
    print("#defines are:")
    for define in cppHeader.defines:
        print("%s" % define)
        s = str(define).split(' ')
        data_set[s[0]] = s[1].strip('"')

    if not os.path.exists(save_dir):
        os.makedirs(save_dir)
    with open(os.path.join(save_dir, file_name), "w+") as f:
        json.dump(data_set, f, indent=4, sort_keys=True)


def run_cmake() -> None:
    execute_cmake = ["cmake"]
    if os.path.exists("build_version.txt"):
        with open("build_version.txt", "r") as fv:
            execute_cmake.append("-DBUILD_VERSION=" + fv.read())

    execute_cmake.append("-DCMAKE_BUILD_TYPE=" + env.get("BOARD"))
    execute_cmake.append("-P")
    execute_cmake.append("CMakeConfigurationValues.txt")
    print(execute_cmake)
    subprocess.call(execute_cmake)


run_cmake()

lib_name = "BuildConfig"
config_lib_path = ""
lib_dirs = env.get("LIBSOURCE_DIRS")
for lib_dir in lib_dirs:
    result = find_library(os.path.realpath(lib_dir), lib_name)
    if result != "":
        config_lib_path = result
        break
print("config_lib_path: " + config_lib_path)

if config_lib_path == "":
    raise Exception("No lib {}".format(lib_name))

path_to_target = os.path.join(config_lib_path, "targets", env.get("BOARD"))
path_to_config_header = os.path.join(path_to_target, "project_config_macros.h")
print('Path to header: ' + path_to_config_header)

header_to_json(path_to_config_header, path_to_target, "cmake_build_config.json")

env.Append(CMAKE_CONFIG_DIR=
    os.path.realpath(path_to_target)
)

# TODO: maybe make some problems when run build without explicit selected board
env.Append(CMAKE_CONFIG_FILE=
    os.path.realpath(os.path.join(path_to_target, "cmake_build_config.json"))
)
