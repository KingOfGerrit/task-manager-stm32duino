#include "project_config.h"

#include "project_config_macros.h"

namespace project_cfg
{

const int build_version = BUILD_VERSION;

const int bluetooth_global_tx = BLUETOOTH_GLOBAL_TX;
const int bluetooth_global_rx = BLUETOOTH_GLOBAL_RX;
const int bluetooth_global_serial_speed = BLUETOOTH_GLOBAL_SERIAL_SPEED;
const int bluetooth_global_size_of_json = BLUETOOTH_GLOBAL_SIZE_OF_JSON;

const int task_manager_task_count = TASK_MANAGER_TASK_COUNT;

const char *rtc_module = RTC_MODULE;

}
