=======================
 General documentation
=======================
-----------
 test
-----------

Used docs and devices
=====================
https://learn.adafruit.com/adafruit-tca9548a-1-to-8-i2c-multiplexer-breakout/wiring-and-test

https://github.com/WeActTC/MiniF4-STM32F4x1

https://docs.platformio.org/en/latest/platforms/ststm32.html

https://github.com/stm32duino/wiki/wiki/Upload-methods

http://www.count-zero.ru/2020/micropython

https://github.com/mcauser/WEACT_F411CEU6/blob/master/README.md

https://3v3.com.ua/product_8434.html

https://learn.adafruit.com/adafruit-hallowing/using-spi-flash

https://github.com/stm32duino/STM32RTC

https://www.arduino.cc/reference/en/libraries/virtmem

http://cdn.compacttool.ru/images/docs/5db835178fd8f.jpg

https://www.norwegiancreations.com/2018/10/arduino-tutorial-avoiding-the-overflow-issue-when-using-millis-and-micros

https://www.programmersought.com/article/56694286013/

https://lesson.iarduino.ru/page/nastroyka-bluetooth-moduley-hc-06-hc-05-ble4-0/

https://dronebotworkshop.com/multiple-i2c-bus/

http://www.learningaboutelectronics.com/Articles/Multiple-SPI-devices-to-an-arduino-microcontroller.php

https://www.hackster.io/arjun/nrf24l01-with-attiny85-3-pins-74a1f2
