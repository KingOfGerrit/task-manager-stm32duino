#pragma once

#include <Task.hpp>

class BluetoothRead : public Task
{
public:
	explicit BluetoothRead(CountOfExecute countOfExecute = CountOfExecute::Forever);

	void run() override;
};
